# Release 0.3.4 - Mon Aug 18 11:06:19 EST 2014

- Added some querysets

# Release 0.3.3 - Wed Aug 13 08:45:18 EST 2014

- Added Ad approval status field to model

# Release 0.3.2 - Thu Aug  7 11:34:15 EST 2014

- Changed some field names, changed tests to use gz files

# Release 0.3.1 - Fri Aug  1 13:55:09 EST 2014

- Changed top ads adgroup by conversion
- Added custom sync tasks

# Release 0.3.0 - Thu Jul 31 15:21:22 EST 2014

- Bug fix
- Added more queryset filters
- Changed report downloaded to use csv.gz
- Added queryset helpers to AdGroup
- Changed sync celery queue.
- 'account_metrics' getter now 'metrics' to follow convention.
- Added 'with_period' queryset
- Removed get_ from queryset filters
- Refs #1769: Added get data queryset methods
- Updated to use googleads 2.0.0
- Made some changes, error handling, model changes
- Refactored a bit of stuff and added more last synced dates for different aspects

# Release 0.2.0 - Mon Jul 28 14:23:35 EST 2014

- Support for Google Adwords v201402

# Release 0.1.0 - Mon Jul 21 10:55:51 EST 2014

- Initial release

